#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: 
# @file: test_swarm.py
# @time: 2018/7/30 17:07
# @Software: PyCharm

import numpy as np
import unittest
from slapy.swarm.pso import *


class Test_graph(unittest.TestCase):
    def test_swarm_pso__1(self):
        def fun(x):
            x, y = x
            if 0 < x < 1 and 0 < y < 1:
                return x + 2 * y - x * y + np.sin(x * y)
            else:
                return -np.inf

        engine = PSOEngine(vmax=0.01, dim=2, fitness_function=fun)
        engine.run()

    def test_graph_util_node_2(self):
        pass

    def test_graph_util_edge_1(self):
        pass

    def test_graph_util_edge_2(self):
        pass

    def test_graph_util_directedgraph_1(self):
        pass

    def test_graph_util_directedgraph_2(self):
        pass
