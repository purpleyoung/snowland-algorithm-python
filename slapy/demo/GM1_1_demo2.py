#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: GM1_1_demo2.py
# @time: 2018/9/4 16:50
# @Software: PyCharm

from slapy.prediction import GM_1_1_model
import numpy as np

# 使用前请安装文件
# pip install snowland-algorithm
# 确保版本号大于0.0.5

npa = np.array

if __name__ == '__main__':
    x = npa([896.77, 992.15, 1096.55, 1203.33, 1358.23, 1598.78, 1823.21, 2094.07, 2466.19, 3006.70])
    lmd = x[:-1] / x[1:]
    print("级比：", lmd)
    gm = GM_1_1_model(x)
    g = gm.predict(3)
    print("预测值：", g)
    gm.draw()
    print("残差：", gm.epsilon())
    print("相对残差：", gm.delta())
    print("级比偏差：", gm.rho())
