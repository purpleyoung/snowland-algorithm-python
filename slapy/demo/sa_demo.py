#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: sa_demo.py
# @time: 2018/8/28 15:39
# @Software: PyCharm

from slapy.swarm.sa import SAEngine
import numpy as np


# 使用前请安装文件
# pip install snowland-algorithm
# 确保版本号大于0.0.4


def fun(vars):
    x, y = vars
    if 0 <= x <= 2 * np.pi and 0 <= y <= 2 * np.pi:
        return -np.cos(x) - np.sin(y) + 10
    else:
        return -10  # 返回一个达不到的小值


if __name__ == '__main__':
    engine = SAEngine(fitness_function=fun, r=0.5, k=1, steps=200)
    engine.run()
