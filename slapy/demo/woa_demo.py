#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: 
# @file: gso_demo.py
# @time: 2018/8/7 16:19
# @Software: PyCharm


from slapy.swarm.woa import WOAEngine
import numpy as np


# 使用前请安装文件
# pip install snowland-algorithm
# 确保版本号大于0.0.3

# 求 z = -cos(x) - sin(y)(x in [1, 2*pi], y in [1, 2*pi]) 的最大值

def fun(vars):
    # 评价函数
    x, y = vars
    if 0 <= x <= 2 * np.pi and 0 <= y <= 2 * np.pi:
        return -np.cos(x) - np.sin(y) + 10
    else:
        return -10  # 返回一个达不到的小值


if __name__ == '__main__':
    engine = WOAEngine(population_size=10, bound=[[0, 2 * np.pi]], min_fitness_value=-np.inf, dim=2, l0=1, fitness_function=fun,
                       steps=300)
    engine.run()
    x, y = engine.gbest.chromosome
    print('计算得到的最大值', fun(engine.gbest.chromosome))
    print('x取值是:', x, 'y取值是:', y)
