#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: pso_demo4.py
# @time: 2018/8/27 11:47
# @Software: PyCharm


from slapy.swarm.pso import PSOEngine
import numpy as np


# 使用前请安装文件
# pip install snowland-algorithm
# 确保版本号大于0.0.2

# 求 max = x + y
# st. x^2 + y^2 \le 1
# x > 0
# y > 0
#

def fun(vars):
    # 评价函数
    x, y = vars
    if 0 <= x <= 1 and 0 <= y <= 1:
        if x ** 2 + y ** 2 > 1:
            return -2
        return x + y
    else:
        return -2  # 返回一个达不到的小值


if __name__ == '__main__':
    engine = PSOEngine(vmax=0.01, bound=[[-1, 1]], min_fitness_value=-1, dim=2, fitness_function=fun, steps=100)
    engine.run()
    x, y = engine.gbest.chromosome
    print('计算得到的最大值是', fun(engine.gbest.chromosome))
    print('x取值是:', x, 'y取值是:', y)
