#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: __init__.py.py
# @time: 2018/9/4 18:23
# @Software: PyCharm


from slapy.swarm.foa.Fly import Fly
from slapy.swarm.foa.FOAEngine import FOAEngine
"""
果蝇优化算法
"""