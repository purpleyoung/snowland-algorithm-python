#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: __init__.py.py
# @time: 2018/9/9 0:47
# @Software: PyCharm


from slapy.swarm.sfla import Frog
from slapy.swarm.sfla import SFLAEngine
"""
蛙跳算法
"""