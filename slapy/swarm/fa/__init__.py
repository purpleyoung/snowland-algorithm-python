#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: __init__.py.py
# @time: 2018/8/16 18:13
# @Software: PyCharm

from slapy.swarm.fa.Firefly import Firefly
from slapy.swarm.fa.FAEngine import FAEngine
# 参考资料：https://blog.csdn.net/u013337691/article/details/52487392?fps=1&locationNum=3
"""
萤火虫优化算法
"""